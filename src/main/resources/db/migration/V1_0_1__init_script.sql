INSERT INTO category ( id, name, version,  created_date, last_updated_date)
VALUES ( 1, 'Konut', 0, now(), null ),
       ( 2, 'Arsa', 0, now(), null ),
       (3, 'Tarla', 0, now(), null);

INSERT INTO city ( id, name, version,  created_date, last_updated_date)
VALUES ( 6, 'Ankara', 0, now(), null ),
       ( 34, 'Istanbul', 0, now(), null ),
       ( 35, 'Izmir', 0, now(), null);