CREATE SCHEMA zapu;

create table category
(
    id                         int            not null primary key,
    name                       varchar(255)    not null,
    version                    int            not null,
    created_date               datetime       not null,
    last_updated_date          datetime       null
);

create table city
(
    id                         int            not null primary key,
    name                       varchar(36)    not null,
    version                    int            not null,
    created_date               datetime       not null,
    last_updated_date          datetime       null
);

create table property
(
    id                         int            not null primary key,
    title                      varchar(255)   not null,
    city_id                    int            not null,
    category_id                int            not null,
    version                    int            not null,
    created_date               datetime       not null,
    last_updated_date          datetime       null,
    foreign key (city_id) references city(id),
    foreign key (category_id) references category(id)
);
