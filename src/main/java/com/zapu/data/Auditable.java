package com.zapu.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode (of = {"id"})
@MappedSuperclass
@EntityListeners (AuditingEntityListener.class)
@SuperBuilder
public class Auditable<I extends Serializable> implements Persistable<I>, Identifier<I> {

	private static final long serialVersionUID = 97255861030246856L;

	public static final Integer INITIAL_VERSION = 0;

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column (nullable = false, unique = true, updatable = false)
	private I id;

	@Column (name = "version")
	@Version
	private Integer version = INITIAL_VERSION;

	@Column (name = "created_date")
	@CreatedDate
	@Temporal (TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column (name = "last_updated_date")
	@LastModifiedDate
	@Temporal (TemporalType.TIMESTAMP)
	private Date lastUpdatedDate;

	// transient fields which specifies not persisted fields
	// default value is false
	@Transient
	protected boolean isNew;
}
