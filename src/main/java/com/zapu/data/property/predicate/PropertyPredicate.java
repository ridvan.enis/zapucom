package com.zapu.data.property.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.zapu.data.property.model.QProperty;
import com.zapu.data.search.model.SearchDTO;
import org.springframework.util.ObjectUtils;

public final class PropertyPredicate {

	public static Predicate createPredicate(SearchDTO searchDTO) {
		BooleanBuilder mainQuery = new BooleanBuilder();
		if(!ObjectUtils.isEmpty(searchDTO.getCategory())){
			mainQuery.and(QProperty.property.category.id.eq(searchDTO.getCategory()));
		}

		if(!ObjectUtils.isEmpty(searchDTO.getCity())){
			mainQuery.and(QProperty.property.city.id.eq(searchDTO.getCity()));
		}
		return mainQuery;
	}
}
