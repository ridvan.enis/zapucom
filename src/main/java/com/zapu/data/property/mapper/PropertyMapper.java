package com.zapu.data.property.mapper;

import com.zapu.data.category.model.Category;
import com.zapu.data.city.model.City;
import com.zapu.data.property.model.Property;
import com.zapu.data.property.model.PropertyDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PropertyMapper {

	public Property toEntity(PropertyDTO dto) {
		return Property.builder()
				.id(dto.getId())
				.version(dto.getVersion())
				.category(Category.builder().id(dto.getCategory()).build())
				.city(City.builder().id(dto.getCity()).build())
				.currency(dto.getCurrency())
				.price(dto.getPrice())
				.title(dto.getTitle())
				.build();
	}

	public List<Property> toEntity(List<PropertyDTO> dtos) {
		if(CollectionUtils.isEmpty(dtos)){
			Collections.emptyList();
		}
		return dtos.stream().map(dto -> toEntity(dto)).collect(Collectors.toList());
	}

	public PropertyDTO toDTO(Property entity) {
		return PropertyDTO.builder()
				.id(entity.getId())
				.version(entity.getVersion())
				.category(entity.getCategory().getId())
				.city(entity.getCity().getId())
				.currency(entity.getCurrency())
				.price(entity.getPrice())
				.title(entity.getTitle())
				.build();
	}

	public List<PropertyDTO> toDTO(List<Property> entities) {
		if(CollectionUtils.isEmpty(entities)){
			Collections.emptyList();
		}
		return entities.stream().map(entity -> toDTO(entity)).collect(Collectors.toList());
	}
}
