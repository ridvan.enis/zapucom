package com.zapu.data.property.model;

import com.zapu.data.Auditable;
import com.zapu.data.category.model.Category;
import com.zapu.data.city.model.City;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Table (name = "property")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
public class Property extends Auditable<Integer> {

	private static final long serialVersionUID = -9197418279190668869L;

	@OneToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "category_id", referencedColumnName = "id")
	private Category category;
	@OneToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "city_id", referencedColumnName = "id")
	private City city;
	private String title;
	private String currency;
	private BigDecimal price;

}
