package com.zapu.data.property.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (of = "{id}")
@SuperBuilder
public class PropertyDTO {

	private Integer id;
	@NotNull
	private Integer city;
	@NotNull
	private Integer category;
	@NotNull
	private String title;
	@NotNull
	private String currency;
	@NotNull
	private BigDecimal price;
	private Integer version;
}
