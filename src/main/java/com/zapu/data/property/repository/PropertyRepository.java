package com.zapu.data.property.repository;

import com.zapu.data.property.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Integer>, QuerydslPredicateExecutor {

}
