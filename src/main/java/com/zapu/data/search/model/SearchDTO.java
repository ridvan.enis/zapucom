package com.zapu.data.search.model;

import com.zapu.data.property.model.Property;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SearchDTO implements Serializable {

	private Integer category;
	private Integer city;
	private Integer page;
	private List<Property> result;
	private String rootUrl;
}
