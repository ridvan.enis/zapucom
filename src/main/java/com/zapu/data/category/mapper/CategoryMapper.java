package com.zapu.data.category.mapper;

import com.zapu.data.category.model.Category;
import com.zapu.data.category.model.CategoryDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryMapper {

	public Category toEntity(CategoryDTO dto) {
		return Category.builder().name(dto.getName()).version(dto.getVersion()).id(dto.getId()).build();
	}

	public List<Category> toEntity(List<CategoryDTO> dtos) {
		if(CollectionUtils.isEmpty(dtos)){
			Collections.emptyList();
		}
		return dtos.stream().map(dto -> toEntity(dto)).collect(Collectors.toList());
	}

	public CategoryDTO toDTO(Category entity) {
		return CategoryDTO.builder().name(entity.getName()).version(entity.getVersion()).id(entity.getId()).build();
	}

	public List<CategoryDTO> toDTO(List<Category> entities) {
		if(CollectionUtils.isEmpty(entities)){
			Collections.emptyList();
		}
		return entities.stream().map(entity -> toDTO(entity)).collect(Collectors.toList());
	}
}
