package com.zapu.data.category.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (of = "{id}")
@SuperBuilder
public class CategoryDTO {

	private Integer id;
	private String name;
	private Integer version;
}
