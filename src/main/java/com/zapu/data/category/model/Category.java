package com.zapu.data.category.model;

import com.zapu.data.Auditable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Table (name = "category")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@EntityListeners (AuditingEntityListener.class)
public class Category extends Auditable<Integer> {

	private static final long serialVersionUID = 5980492571153656555L;

	@Column (name = "name")
	private String name;
}
