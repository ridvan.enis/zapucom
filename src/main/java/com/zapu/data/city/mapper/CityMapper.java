package com.zapu.data.city.mapper;

import com.zapu.data.city.model.City;
import com.zapu.data.city.model.CityDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CityMapper {

	public City toEntity(CityDTO dto) {
		return City.builder().name(dto.getName()).version(dto.getVersion()).id(dto.getId()).build();
	}

	public List<City> toEntity(List<CityDTO> dtos) {
		if(CollectionUtils.isEmpty(dtos)){
			Collections.emptyList();
		}
		return dtos.stream().map(dto -> toEntity(dto)).collect(Collectors.toList());
	}

	public CityDTO toDTO(City entity) {
		return CityDTO.builder().name(entity.getName()).version(entity.getVersion()).id(entity.getId()).build();
	}

	public List<CityDTO> toDTO(List<City> entities) {
		if(CollectionUtils.isEmpty(entities)){
			Collections.emptyList();
		}
		return entities.stream().map(entity -> toDTO(entity)).collect(Collectors.toList());
	}
}
