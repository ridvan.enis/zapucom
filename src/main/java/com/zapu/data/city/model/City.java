package com.zapu.data.city.model;

import com.zapu.data.Auditable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table (name = "City")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
public class City extends Auditable<Integer> {

	private static final long serialVersionUID = 6630309311869378459L;

	@Column(name = "name")
	private String name;
}
