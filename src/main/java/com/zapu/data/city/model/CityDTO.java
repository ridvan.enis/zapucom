package com.zapu.data.city.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString (callSuper = true)
@EqualsAndHashCode (of = "{id}")
@SuperBuilder
public class CityDTO {
	private Integer id;
	private String name;
	private Integer version;
}
