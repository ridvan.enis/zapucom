package com.zapu.service.category.impl;

import com.zapu.data.category.mapper.CategoryMapper;
import com.zapu.data.category.model.Category;
import com.zapu.data.category.model.CategoryDTO;
import com.zapu.data.category.repository.CategoryRepository;
import com.zapu.service.category.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository repository;
	private final CategoryMapper mapper;

	@Override
	public List<CategoryDTO> findAll() {
		List<Category> allCategories = repository.findAll();
		return mapper.toDTO(allCategories);
	}
}
