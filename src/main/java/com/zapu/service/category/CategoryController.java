package com.zapu.service.category;

import com.zapu.data.category.model.CategoryDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController ("/api/categories")
@RequiredArgsConstructor
public class CategoryController {

	private final CategoryService service;

	@GetMapping
	public List<CategoryDTO> findAll() {
		List<CategoryDTO> categories = service.findAll();
		return categories;
	}
}
