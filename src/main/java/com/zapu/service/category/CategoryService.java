package com.zapu.service.category;

import com.zapu.data.category.model.CategoryDTO;

import java.util.List;

public interface CategoryService {

	List<CategoryDTO> findAll();
}
