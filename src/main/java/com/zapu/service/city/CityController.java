package com.zapu.service.city;

import com.zapu.data.city.model.CityDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController ("/api/cities")
@RequiredArgsConstructor
@Slf4j
public class CityController {

	private CityService service;

	@GetMapping (value = "/")
	public List<CityDTO> findAll() {
		return service.findAll();
	}
}
