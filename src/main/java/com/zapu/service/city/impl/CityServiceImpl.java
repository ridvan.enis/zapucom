package com.zapu.service.city.impl;

import com.zapu.data.city.mapper.CityMapper;
import com.zapu.data.city.model.CityDTO;
import com.zapu.data.city.repository.CityRepository;
import com.zapu.service.city.CityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CityServiceImpl implements CityService {

	private final CityRepository repository;
	private final CityMapper mapper;

	@Override
	public List<CityDTO> findAll() {
		List cities = repository.findAll();
		return mapper.toDTO(cities);
	}
}
