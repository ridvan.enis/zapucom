package com.zapu.service.city;

import com.zapu.data.city.model.CityDTO;

import java.util.List;

public interface CityService {

	List<CityDTO> findAll();
}
