package com.zapu.service.search;

import com.zapu.data.search.model.SearchDTO;
import com.zapu.service.property.PropertyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController ("/arama")
@RequiredArgsConstructor
@Slf4j
public class SearchController {

	private final PropertyService propertyService;

	@PostMapping (value = "/")
	public ModelAndView search(SearchDTO searchDTO, Model model) {
		Pageable pageable = PageRequest.of(searchDTO.getPage(), 25);
		return propertyService.search(searchDTO, pageable);
	}
}
