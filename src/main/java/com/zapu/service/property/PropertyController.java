package com.zapu.service.property;

import com.zapu.data.property.model.PropertyDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController ("/api/propety")
@RequiredArgsConstructor
@Slf4j
public class PropertyController {

	private final PropertyService propertyService;

	@PutMapping ("/")
	public void save(@RequestBody PropertyDTO propertyDTO) {
		propertyService.save(propertyDTO);
	}

	@GetMapping (value = "{id}")
	public String findById(@RequestParam ("id") Integer id, Model model) {
		PropertyDTO propertyDTO = propertyService.findById(id);
		model.addAttribute("property", propertyDTO);
		return "redirect:/detay/" + id;
	}
}
