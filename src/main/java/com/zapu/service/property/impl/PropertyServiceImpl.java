package com.zapu.service.property.impl;

import com.querydsl.core.types.Predicate;
import com.zapu.data.property.mapper.PropertyMapper;
import com.zapu.data.property.model.Property;
import com.zapu.data.property.model.PropertyDTO;
import com.zapu.data.property.predicate.PropertyPredicate;
import com.zapu.data.property.repository.PropertyRepository;
import com.zapu.data.search.model.SearchDTO;
import com.zapu.service.property.PropertyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.StringJoiner;

@Service
@RequiredArgsConstructor
@Slf4j
public class PropertyServiceImpl implements PropertyService {

	private final PropertyRepository repository;
	private final PropertyMapper mapper;

	@Override
	@Transactional
	public PropertyDTO save(PropertyDTO propertyDTO) {
		Property property = mapper.toEntity(propertyDTO);
		Property saved = repository.save(property);
		return mapper.toDTO(saved);
		// Detay saylari icin burada propertyler farkli bir dto donusumu yapilip elasticsearche indexlenebilir.
		// Boylece detay sayfalari direk elasticsearch uzerinden okunur ve veritabanina herhangi bir istekle gidilmez.
		// Performans olarak daha iyi bir cozum oldugunu dusunmekteyim
	}

	@Transactional (readOnly = true)
	public PropertyDTO findById(Integer id) {
		Optional<Property> property = repository.findById(id);
		if(property.isEmpty()){
			throw new EntityNotFoundException("property not found!");
		}
		return mapper.toDTO(property.get());
	}

	@Override
	public ModelAndView search(SearchDTO searchDTO, Pageable pageable) {
		Predicate predicate = PropertyPredicate.createPredicate(searchDTO);
		Page<Property> result = repository.findAll(predicate, pageable);

		StringJoiner stringJoiner = new StringJoiner("/");
		if(!CollectionUtils.isEmpty(result.getContent())){
			if(searchDTO.getCategory() != null){
				stringJoiner.add(result.getContent().get(0).getCategory().getName());
			}
			if(searchDTO.getCity() != null){
				stringJoiner.add(result.getContent().get(0).getCity().getName());
			}
		}



		RedirectView rv = new RedirectView();
		rv.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
		rv.setUrl(stringJoiner.toString());

		searchDTO.setResult(result.getContent());
		searchDTO.setRootUrl(stringJoiner.toString());
		return  new ModelAndView(rv);
	}
}
