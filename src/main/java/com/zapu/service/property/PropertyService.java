package com.zapu.service.property;

import com.zapu.data.property.model.PropertyDTO;
import com.zapu.data.search.model.SearchDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.servlet.ModelAndView;

public interface PropertyService {

	PropertyDTO save(PropertyDTO propertyDTO);

	PropertyDTO findById(Integer id);

	ModelAndView search(SearchDTO searchDTO, Pageable pageable);
}
