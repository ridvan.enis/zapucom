package com.zapu.unit;


import com.zapu.data.property.model.PropertyDTO;
import com.zapu.service.property.PropertyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith (SpringExtension.class)
@SpringBootTest
public class PropertyTest {

	@Autowired
	private PropertyService propertyService;

	@Test
	public void create(){
		PropertyDTO property = PropertyDTO.builder().id(1).title("test").city(34).category(1).build();
		PropertyDTO save = propertyService.save(property);
		assertThat(save.getId()).isNotNull();
	}

}
